// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

contract MarkList{

    uint256 public marksCount =0;
    enum Grades{
    A,
    B,
    C,
    D,
    FAIL
}
struct Marks{
    uint cid;
    string code;
    Grades grade;
}


// store marks
mapping(uint =>mapping(string => Marks)) public marks;

event addMarksEvent(
    uint indexed cid,
    string indexed code,
    Grades grades
);

    // event
    event UpdateMarksEvent(
        uint _cid,
        string _code,
        Grades _grade
    );

 function updateMarks(uint _cid, string memory _code, Grades _newgrade) public returns(Marks memory){
    require(bytes(marks[_cid][_code].code).length != 0,"marks is not assigned!" );
    marks[_cid][_code].grade = _newgrade;

    emit UpdateMarksEvent(_cid, _code, _newgrade);
    return marks[_cid][_code];

}

constructor(){
// addMarks(1001,"CSC101,1);
}

function addMarks(uint _cid,string memory _code,Grades _grade) public {
    // require that marks with this cid is not added before
    require(bytes(marks[_cid][_code].code).length ==0, "Marks not found!");
    marksCount++;
    marks[_cid][_code] =Marks(_cid,_code,_grade);

     emit addMarksEvent(marksCount, _code, _grade);
        // return marks[marksCount];
}

  



// fetch marks
function findMarks(uint _cid,string memory _code)
    public view returns (Marks memory)
    {
        return marks[_cid][_code];
    } 

  

}