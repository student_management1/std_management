// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;


contract StudentList{
    
    uint public studentsCount = 0;

    struct Student{
        uint _id;
        uint cid;
        string name;
        bool graduated;
    }

    //store student
    mapping(uint => Student) public students;

    // constructor for students
    // constructor() {
    //     createStudent(1001, "Dorji");
    // }

    //events
    event createStudentEvent(
        uint _id,
        uint indexed cid,
        string name,
        bool graduated
    );

    //Create and add students to storage
    function createStudent(uint _studentCid, string memory _name)
        public returns (Student memory){
            studentsCount++;
            students[studentsCount] = Student(studentsCount, _studentCid, _name, false);

            //trigger
            emit createStudentEvent(studentsCount, _studentCid, _name, false);
            return students[studentsCount];
        }
    
    //event for graduation status
    event markGraduatedEvent(
        uint indexed cid
    );

    //change graduation status of student
    function markGraduated(uint _id)public returns(Student memory){ 
        students[_id].graduated = true;
        //trigger create event
        emit markGraduatedEvent(_id);
        return students[_id];
    }

    //fetch student info from storage
    function findStudent(uint _id) public view returns(Student memory){
        return students[_id];
    }
    event eventUpdateName(uint id);
    function updateStudent(uint _id, string memory _name, uint _cid) public returns(Student memory) {
        students[_id].name=_name;
        students[_id].cid = _cid;
        emit eventUpdateName(_id);
        return students[_id];
    }
     
    

}

