
// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

contract SubjectList{
       uint public subjectsCount = 0;

    struct Subject{
        uint _id;
        string code;
        string name;
        bool retired;
    }

    mapping(uint => Subject) public subjects;

        //events
    event createSubjectEvent(
        uint _id,
        string  code,
        string name,
        bool retired
    );


    // Create and add subject to storage
   function createSubject(string memory _subjectcode, string memory _name )
    public returns(Subject memory){
        subjectsCount++;
        subjects[subjectsCount] = Subject(subjectsCount, _subjectcode, _name, false);

        emit createSubjectEvent(subjectsCount, _subjectcode, _name, false);
        return subjects[subjectsCount];
    }
    // event for retired status
    event markRetiredEvent(
        uint _id
    );

      //change retired
    function markRetired(uint _id)public returns(Subject memory){ 
        subjects[_id].retired = true;
        //trigger create event
        emit markRetiredEvent(_id);
        return subjects[_id];
    }
       //fetch subject info from storage
    function findSubject(uint _id) public view returns(Subject memory){
        return subjects[_id];
    }

    // event
    // UpdatedSubject event is emitted whenever a subject is updated in the updateSubject function.
      event UpdatedSubject(
        uint _id, 
        string code, 
        string name
      );

       function updateSubject(uint _id, string memory _subjectCode, string memory _name) public {
        // updates the subject with the given _id 
  
        //  assigning the new _subjectCode, _name, and _retired to Subject object.
        subjects[_id].name=_name;
        subjects[_id].code=_subjectCode; 

        emit UpdatedSubject(_id, _subjectCode, _name);
        //  emits the UpdatedSubject event with the updated subject's details
    }    
     
}