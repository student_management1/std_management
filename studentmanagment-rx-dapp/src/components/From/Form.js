// import React from "react";
import React, { Component } from 'react'

import './Form.css'

class StudentRecord extends Component {
    render() {
        return (
            <form className="form p-5 border" onSubmit={(event) => {
                event.preventDefault()
                this.props.createStudent(this.sid.value, this.student.value)
            }}>
                <h2>Create Student</h2>

                <input id="newCID" type="text"
                    ref={(input) => { this.sid = input; }}
                    className="form-control m-1"
                    placeholder="SID"
                    required
                />
                <br></br>
                <br></br>
                <input id="" type="text"
                    ref=""
                    className="form-control m-1"
                    placeholder="CID"
                    required
                />
                <br></br>
                <br></br>
                <input id="newStudent" type="text"
                    ref={(input) => {
                        this.student = input;
                    }}
                    className="form-control m-1"
                    placeholder="Student Name"
                    required
                />
                <br></br>
                <br></br>
                {/* <button className='btn btn-info' type="submit" hidden="" style={{ width: "5.5cm" }}>Submit</button> */}
                <input className="form-control btn-primary" type="submit" hidden="" style={{ width: "7cm" }} />
            </form>
        );
    }
}
export default StudentRecord;