import React, { Component } from "react";

class StudentList extends Component {
    render() {
        return (
            <form className="row p-4 row-cols-lg-8 g-2 border 
                 bg-light mt-4 align-items-center" onSubmit={(event) => {
                    event.preventDefault()
                    this.props.createStudent(this.cid.value, this.student.value)
                }}>
                <div className="mb-3">
                    <label className="form-label">Student Cid</label>
                    <input type="" className="form-control" ref={(input) => { this.cid = input; }} placeholder="CID" required />
                </div>
                <div className="mb-3">
                    <label className="form-label">Name</label>
                    <input type="text" className="form-control" ref={(input) => { this.student = input; }} id="exampleInputPassword1" />
                </div>
                <button className='btn btn-info' type="submit" hidden="" style={{ width: "7cm" }}>Submit</button>
            </form>
        )
    }
}

export default StudentList;