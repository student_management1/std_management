// import React from "react";
import React, { Component } from 'react'

import './Form.css'

class SubjectForm extends Component {
    render() {
        return (
            <form className="form p-5 border" onSubmit={(event) => {
                event.preventDefault()
                this.props.createSubject(this.code.value, this.subject.value)
            }}>
                <h6>Create Subject</h6>

                <br></br>
                <br></br>
                <input id="" type="text"
                    ref={(input)=> {this.code = input;}}
                    className="form-control m-1"
                    placeholder="Code"
                    required
                />
                <br></br>
                <br></br>
                <input id="newSubject" type="text"
                    ref={(input)=>{this.subject = input;}}
                    className="form-control m-1"
                    placeholder="Subject Name"
                    required
                />
                <br></br>
                <br></br>
                <button className='btn btn-info' type="submit" hidden="" style={{ width: "7cm" }}>Submit</button>
                {/* <input className="form-control btn-primary" type="submit" hidden="" style={{ width: "7cm" }} /> */}
            </form>
        );
    }
}
export default SubjectForm;