var MarkList = artifacts.require("MarkList");
const SubjectList = artifacts.require('SubjectList')
const StudentList = artifacts.require('StudentList')

contract('Marklist', (account) => {
    // make sure contract is deployed
    beforeEach(async () => {
        this.MarkList = await MarkList.deployed()
    })

    it('Marklist, studentList,subjectLsit test', async () => {
        assert.equal(MarkList._json.contractName, "MarkList",
            "marklist contract is correct"
        )
        subjectcontractname = SubjectList._json.contractName;
        assert.equal(subjectcontractname, "SubjectList")
        studentcontractname = StudentList._json.contractName;
        assert.equal(studentcontractname, "StudentList")
    })

    // testing the content in the contract
    it('adding student test', async () => {
        return StudentList.deployed().then(async (instance) => {
            s = instance;
            const names = ["PuruShotam", "Dorji Phuntsho"];
            studentCid = 1;
            for (let i = 0; i < names.length; i++) {
                s.createStudent(studentCid, names[i]);
                studentCid++;
            }
        })
    })
    it('adding subject test', async () => {
        return SubjectList.deployed().then(async (instance) => {
            s = instance;
            const subjects = ["programming_Blockchain", "Dapp_Development"];
            const codes = ["CSB203", "CSB202"];
            id = 1;
            for (let i = 0; i < subjects.length; i++) {
                s.createSubject(codes[i], subjects[i]);
                id++;

            }

        })
    })
    it('testing marks adding', async () => {
        return MarkList.deployed().then((instance) => {
            m = instance;
            return m.addMarks(2, "CSF101", 1).then(async (transaction) => {
                return m.marksCount().then(async (count) => {
                    assert.equal(count, 1)
                    return m.findMarks(2, "CSF101").then(async (marks) => {
                        assert.equal(marks.code, "CSF101")
                        assert.equal(marks.grade, 1)
                    })
                })
            })
        })
    })
    it("test updateMarks", async () => {
        let target = await markList.findMarks(0, 'ELE202')
        assert.equal(target.cid, 0, "check Cid")
        assert.equal(target.code, "ELE202")
        assert.equal(target.code, "ELE202")
        assert.equal(target.grades, 0)
        await markList.updateMarks(0, 'ELE202', MarkList.Grades.C).then(async () => {
            let updatedValue = await markList.findMarks(0, 'ELE202')
            assert.equal(updatedValue.cid, 0, "Check if cid is correct")
            assert.equal(updatedValue.code, "ELE202")
            assert.equal(updatedValue.code, "ELE202")
            assert.equal(updatedValue.grades, 2)

        })

    })
})

function isValidAddress(address) {
    assert.notEqual(address, 0x0)
    assert.notEqual(address, '')
    assert.notEqual(address, null)
    assert.notEqual(address, undefined)
}

