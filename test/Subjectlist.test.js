const SubjectList = artifacts.require('SubjectList')

contract('SubjectList', (account) => {
    beforeEach(async () => {
        this.subjectList = await SubjectList.deployed()
    })

    it('subjectList contract deploy successfully', async () => {
        const address = await this.subjectList.address
        isValidAddress(address)
    })


    it('adding subject', async () => {
        return SubjectList.deployed().then((instance) => {
            s = instance;
            _code = "IFT303"
            return s.createSubject(_code, "ITPrograming");
        }).then((transaction) => {
            isValidAddress(transaction.tx);
            isValidAddress(transaction.receipt.blockHash);
            return s.subjectsCount();
        }).then((count) => {
            assert.equal(count, 1);
            return s.subjects(1)
        }).then((subject) => {
            assert.equal(subject.code, _code)
        })
    })


    it("test find subject", async () => {
        return SubjectList.deployed().then((instance) => {
            s = instance
            return s.createSubject("103", "Programing_Math").then(async (tx) => {
                return s.createSubject("1437", "Programing_Solidity").then(async (tx) => {
                    return s.createSubject("12345", "Frontend_development").then(async (tx) => {
                        return s.createSubject("123456", "Critical_Analysis_Method").then(async (tx) => {
                            return s.createSubject("123", "Blockchain_Development").then(async (tx) => {
                                return s.subjectsCount().then(async (count) => {
                                    assert.equal(count, 6)
                                    return s.findSubject(4).then((subject) => {
                                        assert.equal(subject.name, "Frontend_development")
                                    })
                                })
                            })
                        })
                    })
                })
            })
        })
    })

    it('text marking retired', async () => {
        return SubjectList.deployed().then((instance) => {
            s = instance
            return s.findSubject(1).then((osubject) => {
                assert.equal(osubject.name, "ITPrograming")
                assert.equal(osubject.retired, false)
                return s.markRetired(1).then(async (transaction) => {
                    return s.findSubject(1).then(async (nsubject) => {
                        assert.equal(nsubject.name, "ITPrograming")
                        assert.equal(nsubject.retired, true)
                    })
                })
            })
        })
    })
})

function isValidAddress(address) {
    assert.notEqual(address, 0x0)
    assert.notEqual(address, '')
    assert.notEqual(address, null)
    assert.notEqual(address, undefined)
}
